<?php 
session_start();
	$_SESSION["firstName"] = $_POST['firstName'];
	$_SESSION["lastName"] = $_POST['lastName'];
	$_SESSION["birthMonth"] = strtoupper($_POST['birthMonth']);
	$_SESSION["birthDay"] = $_POST['birthDay'];
	

	if(strlen($_SESSION["firstName"])===0 || strlen($_SESSION["lastName"])===0 || strlen($_SESSION["birthMonth"])===0 || strlen($_SESSION["birthDay"])===0){
		
		$_SESSION['errorMsg'] = "Please fill up the form properly";
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}
	else{
		switch ($_SESSION['birthMonth']) {
			case "JANUARY":
				if($_SESSION['birthDay'] <=20){
				$zodiac = "Capricorn";
				}else if ($_SESSION['birthDay'] >19){
				 $zodiac = ["Aquarius"];
				}
				break;
			case "FEBRUARY":
				if($_SESSION['birthDay'] <=19){
				 $zodiac = ["Aquarius"];
				}else if ($_SESSION['birthDay'] >18){
				 $zodiac = ["Pisces"];
				}
				break;
			case "MARCH":
				if($_SESSION['birthDay'] <=21){
				 $zodiac = ["Pisces"];
				}else if ($_SESSION['birthDay'] >20){
				 $zodiac = ["Aries"];
				}
				break;
			case "APRIL":
				if($_SESSION['birthDay'] <=20){
				 $zodiac = ["Aries"];
				}else if ($_SESSION['birthDay'] >19){
				 $zodiac = ["Taurus"];
				}
				break;
			case "MAY":
				if($_SESSION['birthDay'] <=21){
				 $zodiac = ["Taurus"];
				}else if ($_SESSION['birthDay'] >20){
				 $zodiac = ["Gemini"];
				}
				break;
			case "JUNE":
				if($_SESSION['birthDay'] <=21){
				 $zodiac = ["Gemini"];
				}else if ($_SESSION['birthDay'] >20){
				 $zodiac = ["Cancer"];
				}
				break;
			case "JULY":
				if($_SESSION['birthDay'] <=23){
				 $zodiac = ["Cancer"];
				}else if ($_SESSION['birthDay'] >22){
				 $zodiac = ["Leo"];
				}
				break;
			case "AUGUST":
				if($_SESSION['birthDay'] <=23){
				 $zodiac = ["Leo"];
				}else if ($_SESSION['birthDay'] >22){
				 $zodiac = ["Virgo"];
				}
				break;
			case "SEPTEMBER":
				if($_SESSION['birthDay'] <=23){
				 $zodiac = ["Virgo"];
				}else if ($_SESSION['birthDay'] >22){
				 $zodiac = ["Libra"];
				}
				break;
			case "OCTOBER":
				if($_SESSION['birthDay'] <=23){
				 $zodiac = ["Libra"];
				}else if ($_SESSION['birthDay'] >22){
				 $zodiac = ["Scorpio"];
				}
				break;
			case "NOVEMBER":
				if($_SESSION['birthDay'] <=22){
				 $zodiac = ["Scorpio"];
				}else if ($_SESSION['birthDay'] >21){
				 $zodiac = ["Sagitarius"];
				}
				break;
			case "DECEMBER":
				if($_SESSION['birthDay'] <=22){
				 $zodiac = ["Sagitarius"];
				}else if($_SESSION['birthDay'] >21){
				 $zodiac = ["Capricorn"];
				}
				break;
			default:
		}
		$_SESSION['zodiac'] = $zodiac;
	}
header("Location: ../views/landingpage.php");
 ?>
