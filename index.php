<html>
<head>
	<title>Zodiac finder</title>

	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/lux/bootstrap.css">
</head>
<body class="bg-dark ">
	<h1 class="text-white text-center">Register here</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-light p-4" action="controllers/reg-process.php" method="POST">
			<div class="form-group">
				<label for="name">First Name</label>
				<input type="text" name="firstName" class="form-control">
			</div>
			<div class="form-group">
				<label for="name">Last Name</label>
				<input type="text" name="lastName" class="form-control">
			</div>
			<div class="form-group">
				<label for="name">Birth month</label>
				<input type="text" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="name">Birth day</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Register</button>
			</div>
			<?php 
				session_start();
				if(isset($_SESSION['errorMsg'])){
			?>
			 <p class="text-black"><?php echo $_SESSION['errorMsg']; ?></p>
			<?php
			session_destroy();
				}
			 ?>
		</form>
	</div>
</body>
</html>